#charset utf8
from django.shortcuts import render, redirect
import datetime
from cargo.models import user, shipment, requestbook
from django.core.mail import send_mail
import json
from django.http import HttpResponse
import sys

# Create your views here.


def shipInfo(request):
    if request.method == 'GET':
        today = datetime.date.today()
        shipmentInfo = shipment.objects.all().order_by('leaveDate')
        s = []
        for i in shipmentInfo:
            s.append([i.shipmentID, 'From ' + i.leaveDate.__str__() + ' To ' + i.arriveDate.__str__()])

        res = {
            'shipInfo': s,
            'status': '0'
        }

        return HttpResponse(json.dumps(res), content_type="application/json")


def signIn(request):
    if request.method == 'GET':
        #print(request.get_full_path(), file=sys.stderr)

        passwordNow = request.GET['password']
        emailNow = request.GET['email']

        #print(passwordNow, file=sys.stderr)
        #print(emailNow, file=sys.stderr)

        check = user.objects.get(email=emailNow)

        if (check.password == passwordNow):
            res = {
                'status': '0',
                'userid': check.userID,
                'usertype': check.userType
            }
        else:
            res = {
                'status': '1'
            }
        return HttpResponse(json.dumps(res), content_type="application/json")


def signUp(request):
    if request.method == 'POST':
        req = json.loads(request.body.decode())

        newUser = user()
        newUser.email = req.get('email')
        newUser.password = req.get('password')
        newUser.userName = req.get('realname')
        newUser.phoneNumber = req.get('phonenumber')
        newUser.homeAddress = req.get('address')

        check = user.objects.filter(email=newUser.email)

        if (check):
            res = {
                'status': 1,
                'msg':'email exist'
            }
            return HttpResponse(json.dumps(res), content_type="application/json")

        else:
            newUser.save()
            res = {
                'status': '0',
                'usertype': '0',
                'userid': newUser.userID,
                'msg': 'success'
            }
            return HttpResponse(json.dumps(res), content_type="application/json")
    else:
        res = {
            'status': '0',
            'msg': 'error access'
        }
        return HttpResponse(json.dumps(res), content_type="application/json")


def profile(request):
    if request.method == 'GET':
        useridNow = int(request.GET['userid'])
        userNow = user.objects.get(userID=useridNow)

        if (userNow):
            res = {
                'status': '0',
                'email': userNow.email,
                'password': userNow.password,
                'realname': userNow.userName,
                'phonenumber': userNow.phoneNumber,
                'address': userNow.homeAddress
            }
            return HttpResponse(json.dumps(res), content_type="application/json")
        else:
            res = {
                'status': '1',
                'msg': 'user not exist'
            }
            return HttpResponse(json.dumps(res), content_type="application/json")


def updateprofile(request):
    if request.method == 'POST':
        req = json.loads(request.body.decode())

        useridNow = int(req.get('userid'))
        userNow = user.objects.get(userID=useridNow)

        userNow.email = req.get('email')
        userNow.password = req.get('password')
        userNow.userName = req.get('realname')
        userNow.phoneNumber = req.get('phonenumber')
        userNow.homeAddress = req.get('address')

        userNow.save()

        res = {
            'status': '0',
            'msg': 'success'
        }
        return HttpResponse(json.dumps(res), content_type="application/json")

def displaybook(request):
    if request.method == 'GET':
        useridNow = int(request.GET['userid'])
        if (int(user.objects.get(userID=useridNow).userType) > 0):
            booklist = requestbook.objects.all()
        else:
            booklist = requestbook.objects.filter(userID_id=useridNow)

        res = []

        for i in booklist:
            select = shipment.objects.get(shipmentID=int(i.shipmentID_id))
            str = 'From ' + select.leaveDate.__str__() + ' To ' + select.arriveDate.__str__()

            res.append({
                'bookingNumber': i.bookID,
                'bookingStatus': i.status,
                'HBLNumber': i.HBL,
                'boxNumber': i.boxNumber,
                'destination': i.destination,
                'pickupAddress': i.pickup,
                'pickupDate': i.pickupDate.__str__().split(' ')[0],
                'selectDate': str,
                'optionalInformationCustomer': i.customerMessage,
                'optionalInformationShipper': i.shiperMessage,
                'cost': i.cost})

        res = {
            'booklist': res,
            'status': '0'
        }
        return HttpResponse(json.dumps(res), content_type="application/json")


def newbook(request):
    if request.method == 'POST':
        req = json.loads(request.body.decode())
        newbook = requestbook()

        newbook.boxNumber = int(req.get('boxnumber'))
        newbook.cost = newbook.boxNumber * 35
        newbook.shipmentID_id = int(req.get('shipmentid'))
        newbook.userID_id = int(req.get('userid'))
        newbook.destination = req.get('destination')
        newbook.pickup = req.get('pickupaddress')
        newbook.customerMessage = req.get('cutomermsg')
        newbook.save()

        res = {
            'status': '0',
            'msg': 'success'
        }
        return HttpResponse(json.dumps(res), content_type="application/json")


def updatebook(request):
    if request.method == 'POST':
        req = json.loads(request.body.decode())

        bookidNow = int(req.get('bookingNumber'))
        bookCur = requestbook.objects.get(bookID=bookidNow)

        bookCur.HBL = req.get('bookingHBL')
        #print(bookCur.HBL, file=sys.stderr)
        bookCur.status = req.get('bookingStatus', bookCur.status)
        bookCur.pickupDate = req.get('bookingPickupDate', bookCur.pickupDate)
        bookCur.shiperMessage = req.get('bookingInformationShipper', bookCur.shiperMessage)

        useridNow = bookCur.userID_id
        bookCur.save()

        userNow = user.objects.get(userID=useridNow)


        send_mail('You booking have been updated.',
                  'Hi ' + userNow.userName + ',\n Your booking status have been changed, please check it.',
                  'susantoonlinecargo@gmail.com',
                  [userNow.email.__str__()],
                  fail_silently=False)

        res = {
            'status': '0',
            'msg': 'success'
        }
        return HttpResponse(json.dumps(res), content_type="application/json")



