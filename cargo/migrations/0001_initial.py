# Generated by Django 2.1.1 on 2018-10-05 05:42

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='request',
            fields=[
                ('bookID', models.AutoField(primary_key=True, serialize=False)),
                ('boxNumber', models.IntegerField()),
                ('destination', models.CharField(max_length=150)),
                ('pickup', models.CharField(max_length=150)),
                ('customerMessage', models.TextField(null=True)),
                ('status', models.IntegerField(default=0, max_length=1)),
                ('cost', models.IntegerField(null=True)),
                ('pickupDate', models.DateTimeField(null=True)),
                ('HBL', models.CharField(max_length=30, null=True)),
                ('shiperMessage', models.TextField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='shipment',
            fields=[
                ('shipmentID', models.AutoField(primary_key=True, serialize=False)),
                ('leaveDate', models.DateField()),
                ('arriveDate', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='user',
            fields=[
                ('userID', models.AutoField(primary_key=True, serialize=False)),
                ('email', models.CharField(max_length=50)),
                ('password', models.CharField(max_length=20)),
                ('userType', models.IntegerField(default=0, max_length=1)),
                ('userName', models.CharField(max_length=30)),
                ('phoneNumber', models.CharField(max_length=20)),
                ('homeAddress', models.CharField(max_length=150)),
            ],
        ),
        migrations.AddField(
            model_name='request',
            name='shipmentID',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cargo.shipment'),
        ),
        migrations.AddField(
            model_name='request',
            name='userID',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cargo.user'),
        ),
    ]
