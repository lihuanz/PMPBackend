from django.db import models

# Create your models here.
class user(models.Model):
    userID = models.AutoField(primary_key=True)
    email = models.CharField(max_length=50)
    password = models.CharField(max_length=20)
    userType = models.IntegerField(default=0)
    userName = models.CharField(max_length=30)
    phoneNumber = models.CharField(max_length=20)
    homeAddress = models.CharField(max_length=150)


# Create your models here.
class shipment(models.Model):
    shipmentID = models.AutoField(primary_key=True)
    leaveDate = models.DateField()
    arriveDate = models.DateField()


# Create your models here.
class requestbook(models.Model):
    bookID = models.AutoField(primary_key=True)
    userID = models.ForeignKey('user', on_delete=models.CASCADE)
    shipmentID = models.ForeignKey('shipment', on_delete=models.CASCADE)
    boxNumber = models.IntegerField()
    destination = models.CharField(max_length=150)
    pickup = models.CharField(max_length=150)
    customerMessage = models.TextField(null=True)
    status = models.IntegerField(default=0)
    cost = models.IntegerField(null=True)
    pickupDate = models.DateField(null=True)
    HBL = models.CharField(max_length=30, null=True)
    shiperMessage = models.TextField(null=True)
