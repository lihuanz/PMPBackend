#from django.http import HttpResponse
from django.shortcuts import render

def homepage(request):
    context = {}
    #context['v1'] = "Hello World!"
    return render(request, 'homePage.html')

def signin(request):
    context = {}
    context['success'] = 'Login Successful'
    return render(request, 'signIn.html', context)

def signup(request):
    return render(request, 'signUp.html')

def newbook(request):
    return render(request, 'newBook.html')

def lookupbook(request):
    return render(request, 'lookUpBook.html')

def updatebook(request):
    return render(request, 'updateBook.html')

def userprofile(request):
    return render(request, 'userProfile.html')

def userupdate(request):
    return render(request, 'userUpdate.html')

