"""PMPBackend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path
from cargo import views as cargoViews

urlpatterns = [
    path('api/user/profile', cargoViews.profile),
    path('api/user/update', cargoViews.updateprofile),
    path('api/user/signin', cargoViews.signIn),
    path('api/user/signup', cargoViews.signUp),
    path('api/booking/check', cargoViews.displaybook),
    path('api/booking/create', cargoViews.newbook),
    path('api/booking/update', cargoViews.updatebook),
    path('api/shipInfo', cargoViews.shipInfo),
]


